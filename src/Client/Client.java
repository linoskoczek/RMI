package Client;

import Remote.Add.AddRequest;
import Remote.Add.AddResponse;
import Remote.Add.IAdd;
import Remote.Echo.EchoRequest;
import Remote.Echo.EchoResponse;
import Remote.Echo.IEcho;
import Remote.Server.ServerConstants;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

public class Client {
    public static void main(String[] args) {
        try {
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new RMISecurityManager());
            }

            Object rmi = Naming.lookup(ServerConstants.RMI_NAME);

            echoRequest((IEcho) rmi, "test");
            echoRequest((IEcho) rmi, "sd';glk d ;lksd l;ga;l kasD?dgas@?#4124?");
            addRequest((IAdd) rmi, 123, 456);
            addRequest((IAdd) rmi, 5, -5);

        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            e.printStackTrace();
        }
    }

    private static void addRequest(IAdd addRMI, int num1, int num2) throws RemoteException, MalformedURLException, NotBoundException {
        //IAdd addRMI = (IAdd) Naming.lookup(ServerConstants.RMI_NAME);
        AddRequest request = new AddRequest(num1, num2);
        AddResponse response = addRMI.add(request);
        System.out.println(response.sum);
    }

    private static void echoRequest(IEcho echoRMI, String message) throws RemoteException, MalformedURLException, NotBoundException {
        //IEcho echoRMI = (IEcho) Naming.lookup(ServerConstants.RMI_NAME);
        EchoRequest request = new EchoRequest(message);
        EchoResponse response = echoRMI.echo(request);
        System.out.println(response.message);

    }
}
