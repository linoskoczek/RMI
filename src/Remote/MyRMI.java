package Remote;

import Remote.Add.AddRequest;
import Remote.Add.AddResponse;
import Remote.Add.IAdd;
import Remote.Echo.EchoRequest;
import Remote.Echo.EchoResponse;
import Remote.Echo.IEcho;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MyRMI extends UnicastRemoteObject implements IEcho, IAdd {
    public MyRMI() throws RemoteException {
    }

    @Override
    public EchoResponse echo(EchoRequest request) throws RemoteException {
        return new EchoResponse(request.message);
    }

    @Override
    public AddResponse add(AddRequest request) throws RemoteException {
        return new AddResponse(request.num1 + request.num2);
    }
}
