package Remote.Echo;

import java.io.Serializable;
import java.rmi.RemoteException;

public class EchoResponse implements Serializable {
    public String message;

    public EchoResponse(String message) throws RemoteException {
        this.message = message;
    }
}
