package Remote.Echo;

import java.io.Serializable;
import java.rmi.RemoteException;

public class EchoRequest implements Serializable {
    public String message;

    public EchoRequest(String message) throws RemoteException {
        this.message = message;
    }
}
