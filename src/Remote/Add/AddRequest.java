package Remote.Add;

import java.io.Serializable;
import java.rmi.RemoteException;

public class AddRequest implements Serializable {
    public int num1, num2;

    public AddRequest(int num1, int num2) throws RemoteException {
        this.num1 = num1;
        this.num2 = num2;
    }
}
