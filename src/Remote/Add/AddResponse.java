package Remote.Add;

import java.io.Serializable;
import java.rmi.RemoteException;

public class AddResponse implements Serializable {
    public int sum;

    public AddResponse(int sum) throws RemoteException {
        this.sum = sum;
    }
}
