package Remote.Server;

import Remote.MyRMI;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServer {

    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(ServerConstants.RMI_PORT);
            MyRMI myRmi = new MyRMI();
            System.out.println("Started");
            Naming.bind(ServerConstants.RMI_NAME, myRmi);
        } catch (RemoteException | MalformedURLException | AlreadyBoundException e) {
            e.printStackTrace();
        }
    }
}
